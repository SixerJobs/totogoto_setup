from ottopy import get_robo_builder

def test(world):
    for x in range(7):
        world.add_flag(2+x,1)
        world.add_flag(8,2+x)
        world.add_flag(1+x, 8)
        world.add_flag(1,1+x)
    world.add_flag_count_goal(28)

get_robo = get_robo_builder(levels={
    'test': test,
})